#pragma once

#include <KIO/SlaveBase>
#include <QMimeDatabase>

#include "ipfsrpc.h"

namespace QCoro {
template<typename T> class Task;
} // namespace QCoro

class IPFSSlave : public KIO::SlaveBase {
public:
    IPFSSlave(const QByteArray &poolSocket, const QByteArray &appSocket);
    ~IPFSSlave() override;

public:
    void openConnection() override;
    void closeConnection() override;

    void listDir(const QUrl &url) override;
    void mimetype(const QUrl &url) override;
    void get(const QUrl &url) override;

private:
    QCoro::Task<> doList(const QUrl &url);
    QCoro::Task<> doGet(const QUrl &url);
    QCoro::Task<> doMimetype(const QUrl &url);


    IPFSIPC mIpfs;
    QMimeDatabase mMimeDb;
};
