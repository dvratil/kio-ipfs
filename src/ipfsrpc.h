#pragma once

#include <QString>
#include <QVector>
#include <QUrl>

#include <QNetworkAccessManager>

#include <QCoroAsyncGenerator>
#include <QCoroTask>

#include <QJsonObject>

#include <optional>

class IPFSSlave;

class IPFSIPC {
public:
    explicit IPFSIPC(IPFSSlave &slave);

    struct Link {
        enum class Type {
            Unknown = 0,
            Directory = 1,
            File = 2
        };

        QString hash;
        QString name;
        uint64_t size;
        QString target;
        Type type;
    };
    struct Object {
        QString hash;
        QVector<Link> links;
    };

    QCoro::AsyncGenerator<Object> ls(QString arg);

    QCoro::Task<> cat(QString arg);

    QCoro::Task<std::optional<QJsonObject>> getDag(QString arg);

    struct Stat {
        QString type;
        QString hash;
        uint64_t size;
        uint64_t sizeLocal;
        uint64_t cumulativeSize;
        int blocks;
        bool withLocality;
        bool local;
    };
    QCoro::Task<Stat> stat(const QString &arg);

private:
    IPFSSlave &mSlave;
    QUrl mDaemonUrl;
    QNetworkAccessManager mNam;
};
