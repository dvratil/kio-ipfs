#include "ipfsrpc.h"
#include "log.h"
#include "slave.h"

#include <QUrlQuery>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QMimeDatabase>

#include <QCoroNetworkReply>
#include <QCoroSignal>

#include <memory>
#include <chrono>

using namespace std::chrono_literals;

namespace {

QVector<IPFSIPC::Link> parseLinks(const QJsonArray &a) {
    QVector<IPFSIPC::Link> links;
    links.reserve(a.size());
    for (const auto &linkObjI : a) {
        const auto linkObj = linkObjI.toObject();
        IPFSIPC::Link link;
        link.hash = linkObj[QStringLiteral("Hash")].toString();
        link.name = linkObj[QStringLiteral("Name")].toString();
        link.size = linkObj[QStringLiteral("Size")].toInt();
        link.target = linkObj[QStringLiteral("Target")].toString();
        switch (linkObj[QStringLiteral("Type")].toInt()) {
            case 1:
                link.type = IPFSIPC::Link::Type::Directory;
                break;
            case 2:
                link.type = IPFSIPC::Link::Type::File;
                break;
            default:
                link.type = IPFSIPC::Link::Type::Unknown;
                break;

        }
        links.push_back(std::move(link));
    }

    return links;
}

std::vector<IPFSIPC::Object> parseObjects(const QByteArray &line) {
    std::vector<IPFSIPC::Object> objs;
    QJsonParseError error;
    const auto doc = QJsonDocument::fromJson(line, &error);
    if (error.error != QJsonParseError::NoError) {
        qCWarning(ipfs_LOG, "Failed to parse IPFS response: %s", qUtf8Printable(error.errorString()));
        return objs;
    }

    const auto jsonRoot = doc.object();
    const auto jsonObjects = jsonRoot[QStringLiteral("Objects")].toArray();
    for (const auto &jsonObjI : jsonObjects) {
        const auto jsonObj = jsonObjI.toObject();
        IPFSIPC::Object obj;
        obj.hash = jsonObj[QStringLiteral("Hash")].toString();
        obj.links = parseLinks(jsonObj[QStringLiteral("Links")].toArray());
        objs.push_back(std::move(obj));
    }

    return objs;

}

} // namespace

IPFSIPC::IPFSIPC(IPFSSlave &slave)
    : mSlave(slave), mDaemonUrl{QStringLiteral("http://127.0.0.1:5001")}
{}

QCoro::AsyncGenerator<IPFSIPC::Object> IPFSIPC::ls(QString arg) {
    QUrl url{mDaemonUrl};
    url.setPath(QStringLiteral("/api/v0/ls"));
    url.setQuery({{
        {QStringLiteral("arg"), QString{arg}},
        {QStringLiteral("resolve-type"), QStringLiteral("true")},
        {QStringLiteral("size"), QStringLiteral("true")},
        {QStringLiteral("stream"), QStringLiteral("true")}
    }});
    QNetworkRequest request{url};
    // Default User-Agent not accepted for some reason, let's use a fake one
    request.setHeader(QNetworkRequest::UserAgentHeader, QStringLiteral("kio_ipfs"));
    qDebug() << request.url();
    // Send POST without body
    auto reply = std::unique_ptr<QNetworkReply>{mNam.sendCustomRequest(request, "POST")};
    QByteArray data;
    while (true) {
        co_await qCoro(reply.get()).waitForReadyRead(-1);
        data = reply->readLine();
        if (data.isEmpty()) {
            break;
        }

        for (auto object : parseObjects(data)) {
            co_yield object;
        }
    }
}

QCoro::Task<> IPFSIPC::cat(QString arg) {
    QUrl url{mDaemonUrl};
    url.setPath(QStringLiteral("/api/v0/cat"));
    url.setQuery({{
        {QStringLiteral("arg"), QString{arg}},
        {QStringLiteral("progress"), QStringLiteral("true")}
    }});
    QNetworkRequest request{url};
    request.setHeader(QNetworkRequest::UserAgentHeader, QStringLiteral("kio_ipfs"));
    auto reply = std::unique_ptr<QNetworkReply>{mNam.sendCustomRequest(request, "POST")};

    std::size_t size = 0;
    bool sizeSent = false;
    bool mtSent = false;
    QMimeDatabase mimeDb;

    while (!reply->isFinished() || reply->bytesAvailable() > 0) {
        co_await qCoro(reply.get()).waitForReadyRead(-1);
        if (!sizeSent && reply->hasRawHeader("X-Content-Length")) {
            const auto length = reply->rawHeader("X-Content-Length").toLongLong();
            mSlave.totalSize(length);
            sizeSent = true;
        }

        const auto data = reply->readAll();

        if (!mtSent) {
            const auto mt = mimeDb.mimeTypeForData(data);
            if (mt.isValid()) {
                mSlave.mimeType(mt.name());
            }
            mtSent = true;
        }

        size += data.size();
        mSlave.data(data);
        mSlave.processedSize(size);
    }
}

QCoro::Task<std::optional<QJsonObject>> IPFSIPC::getDag(QString arg) {
    QUrl url{mDaemonUrl};
    url.setPath(QStringLiteral("/api/v0/dag/get"));
    url.setQuery({{
        {QStringLiteral("arg"), QString{arg}},
        {QStringLiteral("output-codec"), QStringLiteral("dag-json")}
    }});
    QNetworkRequest request{url};
    request.setHeader(QNetworkRequest::UserAgentHeader, QStringLiteral("kio_ipfs"));
    auto reply = std::unique_ptr<QNetworkReply>{mNam.sendCustomRequest(request, "POST")};
    co_await qCoro(reply.get()).waitForFinished();
    QJsonParseError error;
    const auto document = QJsonDocument::fromJson(reply->readAll(), &error);
    if (error.error != QJsonParseError::NoError) {
        qCWarning(ipfs_LOG, "Failed to parse IPFS response: %s", qUtf8Printable(error.errorString()));
        co_return std::nullopt;
    }

    co_return document.object();
}
