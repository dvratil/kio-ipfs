#pragma once

#include <QStringView>

namespace IPFSRPC::Literals {

QStringView operator ""_qsv(const char16_t *str, std::size_t len) {
    return QStringView{str, static_cast<qsizetype>(len)};
}


} // namespace IPFSRPC::Literals
