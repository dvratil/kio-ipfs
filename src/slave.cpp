#include <QObject>
#include <QCoreApplication>
#include <QMimeDatabase>

#include <QCoroTask>

#include "slave.h"
#include "log.h"
#include "literals.h"

using namespace IPFSRPC::Literals;

class KIOPluginForMetadata : public QObject
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.kde.kio.slave.ipfs" FILE "ipfs.json")
};

extern "C" {
int Q_DECL_EXPORT kdemain(int argc, char **argv) {
    QCoreApplication app(argc, argv);
    app.setApplicationName(QStringLiteral("kio_ipfs"));

    IPFSSlave slave(argv[2], argv[3]);
    slave.dispatchLoop();
    return 0;
}
} // extern "C"

IPFSSlave::IPFSSlave(const QByteArray &poolSocket, const QByteArray &appSocket)
    : KIO::SlaveBase("ipfs", poolSocket, appSocket)
    , mIpfs(*this)
{
    // TODO
}

IPFSSlave::~IPFSSlave()
{
    closeConnection();
}

void IPFSSlave::openConnection() {
    qCDebug(ipfs_LOG, "Opening connection to IPFS daemon");
}

void IPFSSlave::closeConnection() {
    qCDebug(ipfs_LOG, "Closing connection to IPFS daemon");
}

void IPFSSlave::listDir(const QUrl &url) {
    QCoro::waitFor(doList(url));
    finished();
}

void IPFSSlave::mimetype(const QUrl &url) {
    QCoro::waitFor(doMimetype(url));
    finished();
}

void IPFSSlave::get(const QUrl &url) {
    QCoro::waitFor(doGet(url));
    finished();
}

QCoro::Task<> IPFSSlave::doList(const QUrl &url) {
    const auto paths = url.path().split(QLatin1Char('/'));
    if (paths.empty()) {
        co_return;
    }

    qCDebug(ipfs_LOG, "list %s", qUtf8Printable(url.toString()));
    QCORO_FOREACH(const auto &obj, mIpfs.ls(paths.last())) {
        for (const auto &link : obj.links) {
            KIO::UDSEntry entry;
            entry.fastInsert(KIO::UDSEntry::UDS_NAME, link.name);
            entry.fastInsert(KIO::UDSEntry::UDS_DISPLAY_NAME, link.name);
            if (link.type == IPFSIPC::Link::Type::Directory) {
                entry.fastInsert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFDIR);
                entry.fastInsert(KIO::UDSEntry::UDS_MIME_TYPE, QStringLiteral("inode/directory"));
            } else {
                entry.fastInsert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFREG);
            }
            entry.fastInsert(KIO::UDSEntry::UDS_SIZE, link.size);
            entry.fastInsert(KIO::UDSEntry::UDS_URL, QUrl{QStringLiteral("ipfs:////%1/%2").arg(url.path().mid(1), link.hash)}.toString());
            entry.fastInsert(KIO::UDSEntry::UDS_ACCESS, S_IREAD);
            listEntry(entry);
        }
    }

    // Finally, create UDS entry for "."
    KIO::UDSEntry entry;
    entry.fastInsert(KIO::UDSEntry::UDS_NAME, QStringLiteral("."));
    entry.fastInsert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFDIR);
    entry.fastInsert(KIO::UDSEntry::UDS_SIZE, 0);
    entry.fastInsert(KIO::UDSEntry::UDS_ACCESS, S_IREAD | S_IEXEC);
    listEntry(entry);
}

QCoro::Task<> IPFSSlave::doMimetype(const QUrl &url) {
    qCDebug(ipfs_LOG, "mimetype %s", qUtf8Printable(url.toString()));

    const auto paths = url.path().split(QLatin1Char('/'));
    if (paths.empty()) {
        co_return;
    }

    const auto linksDag = co_await mIpfs.getDag(paths.last());
    if (!linksDag.has_value()) {
        co_return;
    }

    const auto dataCid = (*linksDag)[u"Links"_qsv][0][u"Hash"_qsv][u"/"_qsv].toString();
    if (dataCid.isNull()) {
        co_return;
    }


    const auto dataDag = co_await mIpfs.getDag(dataCid);
    if (!dataDag.has_value()) {
        co_return;
    }

    const auto dataBase64 =  (*dataDag)[u"Data"_qsv][u"/"_qsv][u"bytes"_qsv].toString();
    const auto decoded = QByteArray::fromBase64(dataBase64.leftRef(256).toLatin1());

    const auto mt = mMimeDb.mimeTypeForData(decoded);
    if (mt.isValid()) {
        qCDebug(ipfs_LOG, "Detected mimetype %s from data", qUtf8Printable(mt.name()));
        mimeType(mt.name());
    }
}

QCoro::Task<> IPFSSlave::doGet(const QUrl &url) {
    qCDebug(ipfs_LOG, "get %s", qUtf8Printable(url.toString()));

    const auto paths = url.path().split(QLatin1Char('/'));
    if (paths.empty()) {
        co_return;
    }

    co_await mIpfs.cat(paths.last());
}

#include "slave.moc"
